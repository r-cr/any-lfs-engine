lfs_install_pass1()
{
    mkdir -p ${T}/preinstall/
    archives="$(pack_name_file)"
    for pack in ${archives} ; do
        tar xfp ${PACKDIR}/${pack} -C ${T}/preinstall/

        if [ -d ${T}/preinstall/${PREFIX}/lib64/ ] ; then
            mkdir -p ${T}/preinstall/${PREFIX}/lib/
            mv ${T}/preinstall/${PREFIX}/lib64/* ${T}/preinstall/${PREFIX}/lib/
            rmdir ${T}/preinstall/${PREFIX}/lib64/
            ln -s lib ${T}/preinstall/${PREFIX}/lib64
        fi

        cp -pRf ${T}/preinstall/${LFS}/* ${LFS}/
    done
}

lfs_extract()
{
    archives="$(pack_name_file)"
    for pack in ${archives} ; do
        tar xfp ${PACKDIR}/${pack} -C /
    done
}

lfs_postinstall()
{
    scripts="$(pack_name)"
    for pack in ${scripts} ; do
        if test -x /var/lib/pkg/scripts/${pack} ; then
            /var/lib/pkg/scripts/${pack}
        fi
    done
}

lfs_install()
{
    lfs_extract
    lfs_postinstall
}

lfs_setup_shell()
{
    set +h
    umask 022
}

pkg_root()
{
    return
}

pkg_context()
{
    return
}

pkg_install()
{
    image_remove_la
    image_strip_bin
}
