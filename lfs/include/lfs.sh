LFS=${RDD_ROOT}
LC_ALL=POSIX
LFS_TGT=$(uname -m)-lfs-linux-gnu
PASS1PREFIX=${lfs_pass1}
LFS_VERSION="${lfs_version}"
export LFS LC_ALL LFS_TGT

if test -x ${PASS1PREFIX}/bin/strip ; then
    STRIP=${PASS1PREFIX}/bin/strip
fi
